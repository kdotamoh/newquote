///// UI Controller

/// UI Variables


// Add NewQuote
var customer = document.getElementById('customer'),
    description = document.getElementById('description'),
    pages = document.getElementById('pages'),
    quoteNo = document.getElementById('quote-no'),
    overage = document.getElementById('overage'),
    quantity = document.getElementById('quantity');


// Size
var finishedFormat = document.getElementById('f-format'),
    pressFormat = document.getElementById('p-format'),
    machine = document.getElementById('machine');


// Insert
var insertPaper = document.getElementById('insert-paper'),
    insertCover = document.getElementById('insert-cover');


// Inks
var inksInsert = document.getElementById('inks-insert'),
    inksCover = document.getElementById('inks-cover');


// Pages
var pagesPerPlate = document.getElementById('p-plate'),
    pagesPerCover = document.getElementById('p-cover');


// Pre-press
var typeSetting = document.querySelector('#type-setting'),
    graphicDesign = document.querySelector('#graphic-design'),
    ctp = document.querySelector('#ctp'),
    prePressCutting = document.querySelector('#prepress-cutting');


// Finishing
var handFolding = document.querySelector('#hand-folding'),
    stahlFolding = document.querySelector('#stahl-folding'),
    handCollating = document.querySelector('#hand-collating'),
    saddleStitching = document.querySelector('#saddle-stitching'),
    gangStitching = document.querySelector('#gang-stitching'),
    perfectBinding = document.querySelector('#perfect-binding'),
    wolenberg = document.querySelector('#wolenberg'),
    trimming = document.querySelector('#trimming'),
    lamination = document.querySelector('#lamination'),
    wrapping = document.querySelector('#wrapping');



/// Events

var addQuote = document.getElementById('add-quote').addEventListener('click', function () {
    quoteDetail = {
        customer: customer.value,
        description: description.value,
        pages: pages.value,
        quoteNo: quoteNo.value,
        overage: overage.value,
        quantity: quantity.value
    }

    if (quoteDetail.pages <= 0 || quoteDetail.quoteNo <= 0 || quoteDetail.overage <= 0 || quoteDetail.quantity <= 0) {
        console.log('Please enter a number greater than 0.')
    }
});

var addSize = document.getElementById('add-size').addEventListener('click', function () {
    sizeDetail = {
        finishedFormat: finishedFormat.selectedIndex,
        pressFormat: pressFormat.selectedIndex,
        machine: machine.selectedIndex,
    }
});

var addInsert = document.getElementById('add-insert').addEventListener('click', function () {
    insertDetail = {
        insertPaper: insertPaper.selectedIndex,
        insertCover: insertCover.selectedIndex,
    }
});

var addInks = document.getElementById('add-inks').addEventListener('click', function () {
    inksDetail = {
        inksInsert: inksInsert.value,
        inksCover: inksCover.value,
    }
});

var addPages = document.getElementById('add-pages').addEventListener('click', function () {
    pagesDetail = {
        pagesPerPlate: pagesPerPlate.selectedIndex,
        inksCover: pagesPerCover.selectedIndex,
    }
});

var addPrePress = document.getElementById('add-prepress').addEventListener('click', function () {
    prePressDetail = {
        typeSetting: typeSetting.checked,
        graphicDesign: graphicDesign.checked,
        ctp: ctp.checked,
        prePressCutting: prePressCutting.checked
    }
});

var addPrePress = document.getElementById('add-prepress').addEventListener('click', function () {
    prePressDetail = {
        typeSetting: typeSetting.checked,
        graphicDesign: graphicDesign.checked,
        ctp: ctp.checked,
        prePressCutting: prePressCutting.checked
    }
});

var addFinishing = document.getElementById('add-finishing').addEventListener('click', function () {
    finishingDetail = {
        handFolding: handFolding.checked,
        stahlFolding: stahlFolding.checked,
        handCollating: handCollating.checked,
        saddleStitching: saddleStitching.checked,
        gangStitching: gangStitching.checked,
        perfectBinding: perfectBinding.checked,
        wolenberg: wolenberg.checked,
        trimming: trimming.checked,
        lamination: lamination.checked,
        wrapping: wrapping.checked
    }
});
